#include "binarytree.h"

#include <assert.h>
#include <stdlib.h>

static BinaryTree *Balance(BinaryTree *tree);
static BinaryTree *LeftRotate(BinaryTree *tree);
static BinaryTree *RightRotate(BinaryTree *tree);

BinaryTree *BinaryTree_New()
{
    BinaryTree *tree = malloc(sizeof(BinaryTree));
    tree->isEmpty = true;
    tree->left = NULL;
    tree->right = NULL;
    tree->balanceFactor = 0;

    return tree;
}

void BinaryTree_Free(BinaryTree *tree)
{
    if (tree == NULL)
    {
        return;
    }

    BinaryTree_Free(tree->left);
    BinaryTree_Free(tree->right);
    free(tree);
}

char *BinaryTree_Search(const BinaryTree *tree, int key)
{
    if (tree == NULL || tree->isEmpty)
    {
        return NULL;
    }

    if (tree->item.key > key)
    {
        return BinaryTree_Search(tree->left, key);
    }

    if (tree->item.key < key)
    {
        return BinaryTree_Search(tree->right, key);
    }

    return tree->item.value;
}

BinaryTree *BinaryTree_Insert(BinaryTree *tree, KeyValuePair item)
{
    if (tree->isEmpty)
    {
        tree->item = item;
        tree->isEmpty = false;
        return tree;
    }

    if (tree->item.key > item.key)
    {
        if (tree->left == NULL)
        {
            tree->left = BinaryTree_New();
        }
        tree->balanceFactor--;
        tree->left = BinaryTree_Insert(tree->left, item);
    }
    else if (tree->item.key < item.key)
    {
        if (tree->right == NULL)
        {
            tree->right = BinaryTree_New();
        }
        tree->balanceFactor++;
        tree->right = BinaryTree_Insert(tree->right, item);
    }
    else
    {
        tree->item = item;
    }

    return Balance(tree);
}

BinaryTree *BinaryTree_Delete(BinaryTree *tree, int key)
{
    return tree;
}

static BinaryTree *Balance(BinaryTree *tree)
{
    if (tree->balanceFactor >= 2)
    {
        if (tree->right->balanceFactor < 0)
        {
            tree->right = RightRotate(tree->right);
        }
        tree = LeftRotate(tree);
    }

    if (tree->balanceFactor <= -2)
    {
        if (tree->left->balanceFactor > 0)
        {
            tree->left = LeftRotate(tree->left);
        }
        tree = RightRotate(tree);
    }

    return tree;
}

static BinaryTree *LeftRotate(BinaryTree *tree)
{
    BinaryTree *child = tree->right;

    child->left = tree;
    child->balanceFactor--;

    tree->right = NULL;
    tree->balanceFactor--;

    return child;
}

static BinaryTree *RightRotate(BinaryTree *tree)
{
    BinaryTree *child = tree->left;

    child->right = tree;
    child->balanceFactor++;

    tree->left = NULL;
    tree->balanceFactor++;

    return child;
}
