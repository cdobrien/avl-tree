#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <stdbool.h>

typedef struct {
    int key;
    char *value;
} KeyValuePair;

typedef struct BinaryTree {
    bool isEmpty;
    int balanceFactor;
    KeyValuePair item;
    struct BinaryTree *left;
    struct BinaryTree *right;
} BinaryTree;

BinaryTree *BinaryTree_New();
void BinaryTree_Free(BinaryTree *tree);

char *BinaryTree_Search(const BinaryTree *tree, int key);
BinaryTree *BinaryTree_Insert(BinaryTree *tree, KeyValuePair item);
BinaryTree *BinaryTree_Delete(BinaryTree *tree, int key);

#endif
