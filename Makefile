CFLAGS = -Wall -Wextra -std=c99 -pedantic -ggdb

tests.binarytree: tests.binarytree.c binarytree.c binarytree.h
	cc $(CFLAGS) -o tests.binarytree tests.binarytree.c binarytree.c

run-tests: tests.binarytree
	valgrind -q --leak-check=full ./tests.binarytree

clean:
	rm -f tests.binarytree *.o

PHONY: clean run-tests
