#include "binarytree.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void Searching_for_a_key_in_an_empty_binary_tree_returns_NULL()
{
    BinaryTree *tree = BinaryTree_New();
    char *result = BinaryTree_Search(tree, 42);
    assert(result == NULL);

    BinaryTree_Free(tree);
}

void Inserting_items_then_searching_for_their_keys_returns_their_values()
{
    char *result;
    KeyValuePair item0 = { .key = 42, .value = "foo" };
    KeyValuePair item1 = { .key = 69, .value = "bar" };

    BinaryTree *tree = BinaryTree_New();
    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);

    result = BinaryTree_Search(tree, item0.key);
    assert(strcmp(result, item0.value) == 0);
    result = BinaryTree_Search(tree, item1.key);
    assert(strcmp(result, item1.value) == 0);

    BinaryTree_Free(tree);
}

void Inserting_an_item_with_a_key_already_in_the_tree_overwrites_the_existing_value()
{
    const int key = 42;
    KeyValuePair item0 = { .key = key, .value = "foo" };
    KeyValuePair item1 = { .key = key, .value = "bar" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);

    char *value = BinaryTree_Search(tree, key);
    assert(strcmp(value, "bar") == 0);

    BinaryTree_Free(tree);
}

void Inserting_an_item_into_the_tree_modifies_the_tree_balance_factor()
{
    KeyValuePair item0 = { .key = 42, .value = "foo" };
    KeyValuePair item1 = { .key = 69, .value = "bar" };
    KeyValuePair item2 = { .key = 13, .value = "baz" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    assert(tree->balanceFactor == 0);
    tree = BinaryTree_Insert(tree, item1);
    assert(tree->balanceFactor == 1);
    tree = BinaryTree_Insert(tree, item2);
    assert(tree->balanceFactor == 0);

    BinaryTree_Free(tree);
}

void Tree_is_balanced_after_item_insertion_creates_balance_factor_2_on_root_and_1_on_child()
{
    KeyValuePair item0 = { .key = 42, .value = "foo" };
    KeyValuePair item1 = { .key = 69, .value = "bar" };
    KeyValuePair item2 = { .key = 72, .value = "baz" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);
    tree = BinaryTree_Insert(tree, item2);

    assert(tree->balanceFactor == 0);

    BinaryTree_Free(tree);
}

void Tree_is_balanced_after_item_insertion_creates_balance_factor_minus_2_on_root_and_minus_1_on_child()
{
    KeyValuePair item0 = { .key = 72, .value = "foo" };
    KeyValuePair item1 = { .key = 69, .value = "bar" };
    KeyValuePair item2 = { .key = 42, .value = "baz" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);
    tree = BinaryTree_Insert(tree, item2);

    assert(tree->balanceFactor == 0);

    BinaryTree_Free(tree);
}

void Tree_is_balanced_after_item_insertion_creates_balance_factor_2_on_root_and_minus_1_on_child()
{
    KeyValuePair item0 = { .key = 42, .value = "foo" };
    KeyValuePair item1 = { .key = 72, .value = "bar" };
    KeyValuePair item2 = { .key = 69, .value = "baz" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);
    tree = BinaryTree_Insert(tree, item2);

    assert(tree->balanceFactor == 0);

    BinaryTree_Free(tree);
}

void Tree_is_balanced_after_item_insertion_creates_balance_factor_minus_2_on_root_and_1_on_child()
{
    KeyValuePair item0 = { .key = 72, .value = "foo" };
    KeyValuePair item1 = { .key = 42, .value = "bar" };
    KeyValuePair item2 = { .key = 69, .value = "baz" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);
    tree = BinaryTree_Insert(tree, item2);

    assert(tree->balanceFactor == 0);

    BinaryTree_Free(tree);
}

void Deleting_an_item_from_the_tree_means_it_cannot_be_found()
{
    KeyValuePair item0 = { .key = 72, .value = "foo" };
    KeyValuePair item1 = { .key = 42, .value = "bar" };

    BinaryTree *tree = BinaryTree_New();

    tree = BinaryTree_Insert(tree, item0);
    tree = BinaryTree_Insert(tree, item1);

    tree = BinaryTree_Delete(tree, item1.key);

    char *found = BinaryTree_Search(tree, item1.key);
    assert(found == NULL);
    BinaryTree_Free(tree);
}

int main()
{
    Searching_for_a_key_in_an_empty_binary_tree_returns_NULL();
    Inserting_items_then_searching_for_their_keys_returns_their_values();
    Inserting_an_item_with_a_key_already_in_the_tree_overwrites_the_existing_value();

    Inserting_an_item_into_the_tree_modifies_the_tree_balance_factor();
    Tree_is_balanced_after_item_insertion_creates_balance_factor_2_on_root_and_1_on_child();
    Tree_is_balanced_after_item_insertion_creates_balance_factor_minus_2_on_root_and_minus_1_on_child();
    Tree_is_balanced_after_item_insertion_creates_balance_factor_2_on_root_and_minus_1_on_child();
    Tree_is_balanced_after_item_insertion_creates_balance_factor_minus_2_on_root_and_1_on_child();

    Deleting_an_item_from_the_tree_means_it_cannot_be_found();

    printf("All tests passed\n");
    return 0;
}
